import random

NOUNS = ['dog','cat','boy','hammer','ball']
VERBS = ['ate','ran','bludgeoned','stalked']
ADJECTIVES = ['scary','hairy','charming','loud','beautiful','cromulent']

def verb():
    return random.choice(VERBS)

def noun():
    return random.choice(NOUNS)

def articlep():
    if random.randrange(100) > 50:
        return random.choice(['the','a'])
    else:
        return ''

def adjectivestar():
    prob = random.randrange(100)
    if prob < 70:
        return random.choice(ADJECTIVES)+" " + adjectivestar()
    else:
        return ''
    
def noun_phrase():
    """
    article? adjective* noun
    """
    return articlep() + ' ' +  adjectivestar() + ' ' + noun()



def sentence():
    return noun_phrase() + ' ' + verb()
