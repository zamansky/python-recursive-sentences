import random

NOUNS = ['dog','cat','boy','hammer','ball']
VERBS = ['ate','ran','bludgeoned','stalked']


def verb():
    return random.choice(VERBS)

def noun():
    return random.choice(NOUNS)

def sentence():
    return noun()+' '+verb()
